#!/usr/bin/with-contenv bash
##############################################################################################################
#
# Purpose:
#   Library function to help with bash logging. Source this file to use the 'log' function:
#   log [INFO|WARN|DEBUG|ERROR] <message>
#
# Parameters:
#   1 - Log Type - Is normally INFO|WARN|DEBUG|ERROR, but any other value will be cut to 4 characters and
#           transformed to uppercase.
#   2 - Message - the message to print to log. If omitted, the function uses stdin (i.e. redirecting or piping)
#
# Tips:
#   - If both params are omitted, then the function uses stdin AND defaults to type "INFO"
#   - If type is set to "2" then it will be converted to "ERROR" this is useful for shortening redirections
#       (See redirection examples)
#
# Examples:
#   Simple:
#     log INFO "Processing file: ${file}"
#     log ERROR "File did not exist: ${file}"
#
#   Redirection:
#     hdfs dfs -cp ${file} ${archive_dir} &> >(log DEBUG)
#     - This redirects both stdout and stderr from the hdfs cp as a debug log. Useful for debugging commands
#       that you or live service wouldn't always want to print to screen
#
#     hdfs dfs -cp ${file} ${archive_dir} 2> >(log ERROR) > >(log INFO)
#     hdfs dfs -cp ${file} ${archive_dir} 2> >(log 2) > >(log)
#     - These two commands do the same thing. The stderr from the hdfs cp goes to the "ERROR" logging, and
#       the stdout goes to the "INFO" logging
#
# Version History:
# ===============
# Ver   Date        Name                    Description
# ===   ====        ====                    ===========
# 0.1   10/06/2019  James Jennett-Wheeler   Pre-review version
# 1.0   13/06/2019  James Jennett-Wheeler   Added "_log" function to handle the printing of messages so that a
#                                           log file can be created if the LOG_FILE variable is set.
#
##############################################################################################################


# If in terminal, display coloured error flag
if [ -t 1 ] ; then
    col_normal=$'\e[39m'
    col_red=$'\e[91m' 
    col_yellow=$'\e[93m'
    col_cyan=$'\e[96m'
fi

# Obscured function to handle the log printing
function _log {
    local type=${1}
    local message=${2}
    local date
    date="$(date +"%Y-%m-%d %H:%M:%S")"
    local colour=""
    local redirect=""
    
    # Set colours (only populated if in terminal)
    [[ ${type} == "ERROR" ]] && colour=${col_red} && redirect='>&2' # Also set redirect so that errors print to stderr
    [[ ${type} == "WARN" ]] && colour=${col_yellow}
    
    if [[ ${type} == "DEBUG" ]]; then
        [[ ${LOG_LEVEL^^} != "DEBUG" ]] && return # If type is debug, but log level is not, then return before printing
        colour=${col_cyan} # Set debug colour
    fi
    
    # Print a formatted message, date followed by the type square brackets followed by the message.
    eval 'printf "%s [${colour}%-5s${col_normal}] - %s\n" "${date}" "${type^^}" "${message}"' ${redirect} #  Wrapped in eval so that redirect is only handled for error messages.
}

# The function for logging
function log {
    local type=${1:-INFO}
    local message=${2}
    type=${type^^} # Set type to uppercase
    
    if [[ ${type} == "ERROR" ]] || [[ ${type} == "2" ]]; then
        if [[ $# -eq 1 ]]; then # If ERROR but no message param, read from stdin
            while IFS= read -r message; do
                _log "ERROR" "${message}"
            done < /dev/stdin
        else                    # Otherwise use the message param
            _log "ERROR" "${message}"
        fi
    else
        print_type="${type:0:4}"
        [[ ${type} == "DEBUG" ]] && print_type="DEBUG"

        if [[ $# -le 1 ]]; then # If INFO but no message param, read from stdin
            while IFS= read -r message; do
                _log "${print_type}" "${message}"
            done < /dev/stdin
        else                    # Otherwise use the message param
            _log "${print_type}" "${message}"
        fi
    fi
}
