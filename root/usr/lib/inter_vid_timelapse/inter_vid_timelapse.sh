#!/usr/bin/with-contenv bash

SCRIPTS="$(readlink -f "$(dirname "${BASH_SOURCE:-$0}")")"

media_path="/media"
tl_fps="1"
tl_suffix="_TL"

while getopts ":-:" opt; do
    case $opt in
        -) if [[ ${OPTARG} =~ ^(.+)[=](.+)$ ]]; then
                case ${BASH_REMATCH[1]} in
                    tl-fps) tl_fps="${BASH_REMATCH[2]}" ;;
                    tl-suffix) tl_suffix="${BASH_REMATCH[2]}" ;;
                    *)  print_error "Invalid Option: --${OPTARG}"; exit 2 ;;
                esac
           else
                case ${OPTARG} in
                    *)  print_error "Invalid Option: --${OPTARG}"; exit 2 ;;
                esac
           fi ;;
       \?) print_error "Invalid Option: -${OPTARG}"; exit 2 ;;
        :) print_error "Option -${OPTARG} requires an argument"; exit 2 ;;
    esac
done

source ${SCRIPTS}/logging.sh
source ${SCRIPTS}/error_check.sh
source ${SCRIPTS}/time.sh
source ${SCRIPTS}/ffmpeg.sh

date_regex="20[1-9][0-9]-[01][0-9]-[0-3][0-9]"
time_regex="[012][0-9]-[012345][0-9]-[012345][0-9]"

cameras=()
for f in ${media_path}/*; do
    [[ -d ${f} ]] || continue
    cameras+=("$(basename "${f}")")
done
log INFO "Start: Inter Video Timelapse"
for camera in "${cameras[@]}"; do
    log INFO "Processing Camera: ${camera}"

    dates=()
    for date_dir in ${media_path}/${camera}/*; do
        date="$(basename "${date_dir}")"
        [[ -d ${date_dir} ]] && [[ ${date} =~ ^${date_regex}$ ]] || continue
        dates+=(${date})
    done

    for date in "${dates[@]}"; do
        has_snapshots=$(find ${media_path}/${camera}/${date} -maxdepth 1 -type f | grep "/${time_regex}\.jpg$" | head -1 | wc -l)
        [[ ${has_snapshots} -eq 0 ]] && continue
    
        last_tl_time="00-00-00"
        if [[ ${#tl_suffix} -gt 0 ]]; then
            temp_tl_time="$(basename "$(find ${media_path}/${camera}/${date} -maxdepth 1 -type f | grep "/${time_regex}${tl_suffix}\.mp4$" | sort | tail -1 | sed "s/${tl_suffix}.mp4$//")")"
            [[ ${temp_tl_time} =~ ^${time_regex}$ ]] && last_tl_time="${temp_tl_time}"
            log DEBUG "Last TL at '${last_tl_time}'"
        fi
        
        prev_video=""
        videos=()
        while IFS='' read -r video; do
            time="$(basename "${video}" | sed "s/\.mp4//")"
            log DEBUG "Check Video: ${video} @ time: ${time}"
            [[ ${time} < ${last_tl_time} ]] && continue
            if [[ ${#prev_video} -eq 0 ]] || [[ ! -f "${prev_video}" ]]; then
                prev_video="${video}"
                log DEBUG "Set First Video: ${prev_video}"
                continue
            fi
            videos+=("${video}")
        done < <(find ${media_path}/${camera}/${date} -maxdepth 1 -type f | grep "/${time_regex}\.mp4$" | sort 2>/dev/null)
        [[ ${date} < $(date +%Y-%m-%d) ]] && videos+=("to_midnight")


        [[ ${#videos[@]} -gt 0 ]] && [[ -f "${prev_video}" ]] && log INFO "Date: ${date}" || continue
        log DEBUG "First Video: ${prev_video}"
        
        time="$(basename "${prev_video}" | sed "s/\.mp4//")"
        touch -m -d "${date} ${time//-/:}" ${prev_video}
        
        if [[ ${last_tl_time} == "00-00-00" ]]; then
            from_time="${last_tl_time}"
            to_time="$(basename "${prev_video}" | sed "s/\.mp4//")"
            
            timelapse="${media_path}/${camera}/${date}/${from_time}${tl_suffix}.mp4"
            [[ -f ${timelapse} ]] && continue
            log INFO "Timelapse Span: ${from_time} - ${to_time}"
        
            snapshotFiles=()
            while IFS='' read -r snapshotFile; do
                [[ -f "${snapshotFile}" ]] || continue
                ss_time="$(basename "${snapshotFile}" | sed "s/\.jpg//")"
                [[ ${ss_time} =~ ^${time_regex}$ ]] || continue
                [[ ! "${ss_time}" < "${from_time}" ]] && [[ "${ss_time}" < "${to_time}" ]] && snapshotFiles+=("${snapshotFile}")
                [[ "${ss_time}" < "${from_time}" ]] && rm "${snapshotFile}"
                [[ ! "${ss_time}" < "${to_time}" ]] && break
            done < <(find ${media_path}/${camera}/${date} -maxdepth 1 -type f | grep "/${time_regex}\.jpg$" | sort 2>/dev/null)

            if [[ "${#snapshotFiles[@]}" -gt 0 ]]; then
                if snapshotToTimelapse "${timelapse}" "${tl_fps}" "${snapshotFiles[@]}"; then
                    [[ -f "${timelapse}" ]] && rm "${snapshotFiles[@]}"
                    touch -m -d "${date} ${from_time//-/:}" ${timelapse}
                else
                    exit 1
                fi
            fi
        fi
        
        for video in "${videos[@]}"; do
            prev_video_time="$(basename "${prev_video}" | sed "s/\.mp4//")"
            from_seconds="$(timeToSeconds "${prev_video_time}")"
            prev_vid_duration=$(videoDuration "${prev_video}")
            log DEBUG "Duration of ${prev_video}: ${prev_vid_duration}s"
            from_seconds=$(( from_seconds + $(videoDuration "${prev_video}") ))
            from_time=$(secondsToTime ${from_seconds})
            
            to_time=24-00-00
            [[ ${video} != "to_midnight" ]] && to_time="$(basename "${video}" | sed "s/\.mp4//")"
            
            timelapse="${media_path}/${camera}/${date}/${from_time}${tl_suffix}.mp4"
            [[ -f ${timelapse} ]] && continue
            log INFO "Timelapse Span: ${from_time} - ${to_time}"
        
            snapshotFiles=()
            while IFS='' read -r snapshotFile; do
                [[ -f "${snapshotFile}" ]] || continue
                ss_time="$(basename "${snapshotFile}" | sed "s/\.jpg//")"
                [[ ${ss_time} =~ ^${time_regex}$ ]] || continue
                [[ ! "${ss_time}" < "${from_time}" ]] && [[ "${ss_time}" < "${to_time}" ]] && snapshotFiles+=("${snapshotFile}")
                [[ "${ss_time}" < "${from_time}" ]] && rm "${snapshotFile}"
                [[ ! "${ss_time}" < "${to_time}" ]] && break
            done < <(find ${media_path}/${camera}/${date} -maxdepth 1 -type f | grep "/${time_regex}\.jpg$" | sort 2>/dev/null)

            if [[ "${#snapshotFiles[@]}" -gt 0 ]]; then
                if snapshotToTimelapse "${timelapse}" "${tl_fps}" "${snapshotFiles[@]}"; then
                    [[ -f "${timelapse}" ]] && rm "${snapshotFiles[@]}"
                    touch -m -d "${date} ${from_time//-/:}" ${timelapse}
                else
                    exit 1
                fi
            fi
            
            [[ ${video} != "to_midnight" ]] && touch -m -d "${date} ${to_time//-/:}" ${video}
            prev_video="${video}"
        done
    done
done
retval=$?
log INFO "End: Inter Video Timelapse"
exit ${retval}
