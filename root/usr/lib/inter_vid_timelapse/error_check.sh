#!/usr/bin/with-contenv bash

function error_check {
    local retval=$?
    local step=$1
    if [[ ${retval} -ne 0 ]]; then
        log ERROR "${step} - FAILED (Exit: ${retval})"
        exit ${retval}
    else
        log INFO "${step} - SUCCESS"
    fi
}