#!/usr/bin/with-contenv bash

conv_speed=veryfast

function encode {
    local input output retval
    input="${1}"
    output="${2}"
    log INFO "FFMPEG: reprocess '${input}' to '${output}'"
    ! [[ -f ${input} ]] && log ERROR "FFMPEG: Cannot find input (${input})!" && return 1
    [[ ${input} == "${output}" ]] && log ERROR "FFMPEG: Input (${input}) cannot be same as output (${output})!" && return 1
    [[ -f ${output} ]] && log ERROR "FFMPEG: Output (${output}) already exists!" && return 1

#    ffmpeg -nostats -loglevel 0 -i "${input}" -c:v libx264 -crf 18 -level 3.1 -preset ${conv_speed} -c:a copy -y "${output}"
    ffmpeg -nostats -loglevel 0 -i "${input}" -c:v libx264 -preset ${conv_speed} -c:a copy -y "${output}"
    retval=$?
    sleep 1
    if [[ ${retval} -gt 0 ]]; then
        rm -f "${output}"
    fi

    return ${retval}
}

function snapshotToTimelapse {
    local output tempDir framerate retval
    output="${1}"
    framerate="${2}"
    shift 2

    log INFO "FFMPEG: timelapse ${output}"
    tempDir=$(mktemp -d)
    for snapshot in "$@"; do
        log DEBUG "${snapshot}"
        [[ -f ${snapshot} ]] || return 1
        cp "${snapshot}" "${tempDir}"
    done

    ffmpeg -nostats -loglevel 0 -framerate "${framerate}" -f image2 -pattern_type glob -i "${tempDir}/*.jpg" -c:v libx264 -r "${framerate}" -preset ${conv_speed} -y "${output}" 2> /dev/null
    retval=$?
    sleep 1
    cp "${1}" "${output}.thumb"
    rm -rf "${tempDir}" &&
    return ${retval}
}

function videoDuration {
    local input
    input="${1}"
    ! [[ -f ${input} ]] && log ERROR "FFMPEG: Cannot find input (${input})!" && return 1

    duration=$(ffmpeg -i ${input} 2>&1 | grep -oE "Duration: [012][0-9]:[012345][0-9]:[012345][0-9].[0-9]{2}," | sed "s/^Duration: //; s/,$//; s/:/-/g")
    decimal=${duration#*.}
    seconds=$(timeToSeconds ${duration%.??})
    [[ ${decimal#0} -gt 0 ]] && (( seconds++ ))
    echo ${seconds}
}
