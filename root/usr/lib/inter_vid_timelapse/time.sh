#!/usr/bin/with-contenv bash

function timeToSeconds {
    local time="${1}"
    if [[ ${time} =~ ^([012][0-9])-([012345][0-9])-([012345][0-9])$ ]]; then
        local hours=${BASH_REMATCH[1]#0}
        while [[ ${hours} -gt 23 ]]; do hours=$(( hours - 24 )); done
        local minutes=${BASH_REMATCH[2]#0}
        local seconds=${BASH_REMATCH[3]#0}
        local output=$(( ( ( ( hours * 60 ) + minutes ) * 60 ) + seconds ))
#        log DEBUG "Time to Seconds: ${hours}-${minutes}-${seconds} = ${output}" >&2
        echo ${output}
    fi
}

function secondsToTime {
    local input="${1}"

    if [[ ${input} =~ ^[0-9]+$ ]]; then
        local hours_and_minutes=$(( input / 60 ))
        local hours=$(( hours_and_minutes / 60 ))
        local minutes=$(( hours_and_minutes - ( hours * 60 ) ))
        local seconds=$(( input - ( ( ( hours * 60 ) + minutes ) * 60 ) ))
        local output=$(printf "%02d-%02d-%02d" "${hours}" "${minutes}" "${seconds}")
#        log DEBUG "Seconds to Time: ${input} = ${output}" >&2
        echo ${output}
    fi
}
