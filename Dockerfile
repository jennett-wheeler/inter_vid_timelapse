FROM lsiobase/alpine:3.11

RUN \
 echo "**** install runtime packages ****" && \
 echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
 apk add --no-cache ffmpeg && \
 rm -rf /tmp/*

# copy local files
COPY root/ /

# environment variables
ENV TL_FPS="1" \
    TL_SUFFIX="_TL"

VOLUME /media
